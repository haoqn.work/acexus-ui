module.exports = {
  customSyntax: 'postcss-html',
  extends: [
    'stylelint-config-standard',
    'stylelint-config-recommended-vue',
    'stylelint-config-prettier',
  ],
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  rules: {
    "selector-class-pattern": null,
    "at-rule-empty-line-before": ["always"],
    "rule-empty-line-before": ["never", {
      except: ["first-nested"]
    }],
    "color-function-notation": "legacy"
  },
}

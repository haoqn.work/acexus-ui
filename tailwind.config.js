/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '5rem',
        '2xl': '6rem',
      },
    },
    extend: {
      colors: {
        'rgba-black': 'rgba(0, 0, 0, 0.2)',
        'primary-500': '#ea1e00',
        'primary-400': '#ff5e14',
        'black-500': '#282828',
        'blueGray-800': '#1e293b',
        'blueGray-700': '#334155',
        'blueGray-200':'#e2e8f0',
        'blueGray-500': '#64748b'
      },
      fontSize: {
        max: '200px',
        '5xl': '50px'
      },
      transitionTimingFunction: {
        ease: 'ease'
      },
      translate: {
        'y-10': '10px',
      }
    },

  },
  plugins: [],
}
